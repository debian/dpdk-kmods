dpdk-kmods (0~20230205+git-2) unstable; urgency=medium

  * Bump Standards-Version to 4.7.0, no changes
  * Add loong64 to the list of supported architectures (Closes: #1059082)
  * autopkgtest: fix collecting logs when there aren't any (Closes:
    #1069611)
  * autopkgtest: restrict to supported architectures
  * autopkgtest: ensure kernel headers are pulled in
  * d/u/metadata: add bugzilla link

 -- Luca Boccassi <bluca@debian.org>  Sun, 19 May 2024 03:30:16 +0100

dpdk-kmods (0~20230205+git-1) unstable; urgency=medium

  [ Luca Boccassi ]
  * Merge branch 'upstream' into debian/unstable
  * Drop i386 support
  * Drop fix-build-on-Linux-5.18.patch, merged upstream

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

 -- Luca Boccassi <bluca@debian.org>  Wed, 20 Dec 2023 11:44:15 +0100

dpdk-kmods (0~20220829+git-3) unstable; urgency=medium

  [ Bo YU ]
  * Enable build for riscv64 (Closes: #1027334)

  [ Luca Boccassi ]
  * Bump Standards-Version to 4.6.2, no changes

 -- Luca Boccassi <bluca@debian.org>  Fri, 30 Dec 2022 16:47:30 +0100

dpdk-kmods (0~20220829+git-2) unstable; urgency=medium

  * Update fix-build-on-Linux-5.18.patch to upstream version

 -- Luca Boccassi <bluca@debian.org>  Thu, 22 Dec 2022 16:32:44 +0100

dpdk-kmods (0~20220829+git-1) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Switch to dh-sequence-dkms.
  * Declare Testsuite: autopkgtest-pkg-dkms.

  [ Luca Boccassi ]
  * Merge branch 'dkms-updates' into 'debian/unstable'
  * Merge branch 'upstream' into debian/unstable
  * Bump Standards-Version to 4.6.1, no changes
  * Add patch to fix build on Linux 5.18+
  * Bump copyright year ranges in d/copyright

 -- Luca Boccassi <bluca@debian.org>  Fri, 16 Dec 2022 17:23:58 +0000

dpdk-kmods (0~20220111+git-1) unstable; urgency=medium

  * Merge remote-tracking branch 'upstream/main' into debian/unstable

 -- Luca Boccassi <bluca@debian.org>  Sat, 15 Jan 2022 17:10:23 +0000

dpdk-kmods (0~20210930+git-1) unstable; urgency=medium

  * Merge remote-tracking branch 'upstream/main' into debian/unstable
  * Bump Standards-Version to 4.6.0, no changes
  * Bump debhelper-compat to 13, no changes
  * Bump d/copyright year range

 -- Luca Boccassi <bluca@debian.org>  Tue, 21 Dec 2021 20:17:16 +0000

dpdk-kmods (0~20201113+git-2) unstable; urgency=high

  * Upload to unstable.

 -- Luca Boccassi <bluca@debian.org>  Sun, 10 Jan 2021 16:06:01 +0000

dpdk-kmods (0~20201113+git-1) experimental; urgency=medium

  * Initial packaging (Closes: #974216)

 -- Luca Boccassi <bluca@debian.org>  Thu, 19 Nov 2020 16:28:36 +0000
